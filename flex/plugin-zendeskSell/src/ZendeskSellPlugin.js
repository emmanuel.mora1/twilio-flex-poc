import React from 'react';
import { VERSION } from '@twilio/flex-ui';
import { FlexPlugin } from 'flex-plugin';

import { loadScript } from './helpers/load-script';

import reducers, { namespace } from './states';
//import { Flex } from '@twilio/flex-ui/src/FlexGlobal';

const PLUGIN_NAME = 'ZendeskSellPlugin';

let client = null;
let backgroundApp = null;

export default class ZendeskSellPlugin extends FlexPlugin {
  constructor() {
    super(PLUGIN_NAME);
  }

  // onScriptLoaded = (stop: () => void) => {
  //   const zafClientLocation = this.getZafClientLocation();
  //   this.client = ZAFClient.init(null, zafClientLocation);

  //   if (!this.client) {
  //     console.warn(
  //       `Cannot find any Zendesk instance on the page. The ${PLUGIN_NAME} plugin will be disabled.`
  //     );
  //     stop();
  //     return;
  //   }

  //   this.storeZafClientLocation(this.client);
  // };

  //  handler for Click to Dial events
  clickToDial(flex, params) {
    console.log('===== in click to dial ====== ');
    console.log('params', params);
    console.log('phone', params.phone);
    console.log('flex', flex);
    flex.Actions.invokeAction('StartOutboundCall', {
      destination: params.phone,
    });
  }

  //  handler for Click to Dial events
  sendSmsMessage(params) {
    console.log('===== in send SMS ====== ');
    console.log('params', params);
    console.log('phone', params.contactMobile);
    console.log('message', params.message);
  }

  /**
   * This code is run when your plugin is being started
   * Use this to modify any UI components or attach to the actions framework
   *
   * @param flex { typeof import('@twilio/flex-ui') }
   * @param manager { import('@twilio/flex-ui').Manager }
   */

  async init(flex, manager) {
    this.registerReducers(manager);

    //  disable the Panel2 component
    flex.AgentDesktopView.defaultProps.showPanel2 = false;

    //  load SFDC Lightining OPEN CTI script
    const zendeskSellSdkUrl =
      'https://static.zdassets.com/zendesk_app_framework_sdk/2.0/zaf_sdk.min.js';

    await loadScript(zendeskSellSdkUrl).then(async (response) => {
      console.log('====== response ========');
      console.log(response);

      console.log('====== init ========');

      client = window.ZAFClient.init();

      console.log(client);

      // show the Flex UI within ZD
      manager.workerClient.on('reservationCreated', (reservation) => {
        //   display (pop) Flex softphone
        client.invoke('popover', 'show');

        // look for other reservation events - HIDE softphone
        reservationEvents(reservation);
      });

      //   HIDE Flex softphone on task reservation events
      const reservationEvents = (reservation) => {
        //   reservation events
        reservation.on('cancelled', (reservation) => {
          client.invoke('popover', 'hide');
        });
        reservation.on('timeout', (reservation) => {
          client.invoke('popover', 'hide');
        });
        reservation.on('rescinded', (reservation) => {
          client.invoke('popover', 'hide');
        });
      };

      // await this.client.on('app.registered');
      // const context = await this.client.context();

      // if (context.location !== 'top_bar') {
      //   console.warn(
      //     `The Flex instance should be loaded inside "top_bar" location. "${context.location}" received instead.`
      //   );
      //   return;
      // }

      const getInstances = await client.get('instances');
      const clientInstances = getInstances && getInstances.instances;
      console.log('======== instances ==========');
      console.log(clientInstances);

      for (const instance in clientInstances) {
        if (clientInstances[instance].location === 'background') {
          backgroundApp = client.instance(instance);
        }
      }
      console.log('========= backgroundApp ==========');
      console.log(backgroundApp);

      client.on('click_to_dial', this.clickToDial.bind(this, flex));
      client.on('send_sms_message', this.sendSmsMessage.bind(this));
    });
  }

  /**
   * Registers the plugin reducers
   *
   * @param manager { Flex.Manager }
   */
  registerReducers(manager) {
    if (!manager.store.addReducer) {
      // eslint: disable-next-line
      console.error(
        `You need FlexUI > 1.9.0 to use built-in redux; you are currently on ${VERSION}`
      );
      return;
    }

    manager.store.addReducer(namespace, reducers);
  }
}
