import get from 'lodash/get';
import { FlexPlugin } from 'flex-plugin';
import {
  crmIntegration,
  ITask,
  LogDescription,
  MessageStatus,
  EventName,
} from '@twiliointernal/flex-crm-integration-library';

import * as Pkg from '../package.json';

const CRM_SDK_URI = 'https://static.zdassets.com/zendesk_app_framework_sdk/2.0/zaf_sdk.min.js';
const PLUGIN_NAME = `Flex Zendesk Integration Plugin v${Pkg.version}`;

declare var ZAFClient: any;

interface GetTicketResponse {
  ticketId?: string;
  isClosed?: boolean;
}

interface ZAFClient {
  _appGuid: string;
  _origin: string;
}

/**
 * IntegrationPlugin
 */
export class IntegrationPlugin extends FlexPlugin {
  private selectedTicket: any;
  private newTickets: any;
  private newUsers: any;
  private backgroundApp: any;
  private client: any;

  constructor() {
    super(PLUGIN_NAME);
  }

  /**
   * init
   *
   * Conditionally initialize the plugin
   */
  public async init(flex: any) {
    this.selectedTicket = {};
    this.newTickets = {};
    this.newUsers = {};
    this.backgroundApp = undefined;

    try {
      // Initialize the crm integration only if Flex is running inside an iframe
      await crmIntegration.init({
        flex,
        sdkUri: CRM_SDK_URI,
        integrationAuthor: 'Twilio',
        integrationName: 'zendesk',
        showWidget: this.widgetPopup.bind(this),
        isTaskOnScreen: this.isTaskOnScreen.bind(this),
        logToCrm: this.log.bind(this),
        navigate: this.navigate.bind(this),
        onScriptLoaded: this.onScriptLoaded.bind(this),
      });
    } catch (err) {
      console.error(err);
      return;
    }

    await this.client.on('app.registered');
    const context = await this.client.context();

    if (context.location !== 'top_bar') {
      console.warn(
        `The Flex instance should be loaded inside "top_bar" location. "${context.location}" received instead.`
      );
      return;
    }

    const getInstances = await this.client.get('instances');
    const clientInstances = getInstances && getInstances.instances;

    for (const instance in clientInstances) {
      if (clientInstances[instance].location === 'background') {
        this.backgroundApp = this.client.instance(instance);
      }
    }

    this.client.on('click_to_dial', this.clickToDial.bind(this));
    this.client.on('ticket.activated', (context: any) => (this.selectedTicket = context));
    this.client.on('ticket.deactivated', () => (this.selectedTicket = {}));
  }

  private storeZafClientLocation(client: ZAFClient) {
    const url = new URL(window.location.href);
    url.searchParams.set('origin', client._origin);
    url.searchParams.set('app_guid', client._appGuid);

    window.sessionStorage.setItem('flex_zaf_client_url', url.href);
  }

  private getZafClientLocation() {
    const appUrl = new URL(window.location.href);
    const sessionZafClientUrl = new URL(window.sessionStorage.getItem('flex_zaf_client_url') || window.location.href);
    const urlAppGuid = appUrl.searchParams.get('app_guid');
    const urlOrigin = appUrl.searchParams.get('origin');

    if (!(urlAppGuid && urlOrigin)) {
      const sessionAppGuid = sessionZafClientUrl.searchParams.get('app_guid') || '';
      const sessionAppOrigin = sessionZafClientUrl.searchParams.get('origin') || '';
      appUrl.searchParams.set('app_guid', sessionAppGuid);
      appUrl.searchParams.set('origin', sessionAppOrigin);
    }

    return appUrl;
  }

  private onScriptLoaded = (stop: () => void) => {
    const zafClientLocation = this.getZafClientLocation();
    this.client = ZAFClient.init(null, zafClientLocation);

    if (!this.client) {
      console.warn(`Cannot find any Zendesk instance on the page. The ${PLUGIN_NAME} plugin will be disabled.`);
      stop();
      return;
    }

    this.storeZafClientLocation(this.client);
  };

  private widgetPopup() {
    this.client.invoke('popover');
  }

  private async isTaskOnScreen(task: ITask) {
    const taskTicketId = task.attributes['zd_ticket_id'];

    if (this.selectedTicket.ticketId) {
      const getInstances = await this.client.get('instances');
      const clientInstances = getInstances && getInstances.instances;

      const isInstanceStillAlive = Object.keys(clientInstances).find(
        clientInstance => clientInstance === this.selectedTicket.instanceGuid
      );

      if (!isInstanceStillAlive) {
        this.selectedTicket = {};
        return false;
      }

      if (this.selectedTicket.ticketId && this.newTickets[task.taskSid]) {
        return Number(this.selectedTicket.ticketId) === Number(this.newTickets[task.taskSid]);
      }
    }

    const isSameNumber = Number(taskTicketId) === Number(this.selectedTicket.ticketId);
    const navigateToTicket = crmIntegration.getIntegrationParameter(
      isSameNumber ? 'validTicketNumber.navigateToTicket' : 'invalidTicketNumber.navigateToTicket'
    );

    if (isSameNumber && navigateToTicket) {
      // If the ticket is closed, always trigger the navigate logic
      const { isClosed } = await this.getTicket(task.taskSid, taskTicketId);

      return !isClosed;
    }

    return false;
  }

  private async navigate(task: ITask, eventName: EventName) {
    const { taskChannelUniqueName } = task;

    const { zd_ticket_id: zdTicketId, zdCustomTags, direction, to, name, channelType } = task.attributes;

    const viaId = direction === 'outbound' ? 46 : 45;
    const userPhone = direction === 'outbound' ? to : name;
    const userId = await this.getOrCreateUser(channelType, eventName, userPhone);
    const { ticketId, isClosed } = await this.getOrCreateTicket(
      task.sid,
      eventName,
      zdCustomTags || channelType || taskChannelUniqueName,
      viaId,
      userId,
      zdTicketId
    );

    const isValidTicketId = zdTicketId && !isClosed && zdTicketId === ticketId;

    await this.navigateToUserScreen(userId, isValidTicketId);
    await this.navigateToTicketScreen(ticketId, isValidTicketId);
  }

  private async getTicket(taskSid: string, ticketId?: string): Promise<GetTicketResponse> {
    if (this.newTickets[taskSid]) {
      return {
        ticketId: this.newTickets[taskSid],
      };
    }

    if (ticketId) {
      try {
        const res = await this.client.request(`/api/v2/tickets/${ticketId}.json`);
        const ticketStatus = get(res, 'ticket.status');

        return {
          ticketId: ticketStatus === 'closed' ? undefined : ticketId,
          isClosed: ticketStatus === 'closed',
        };
      } catch (err) {
        return {};
      }
    }

    return {};
  }

  private async getOrCreateTicket(
    taskSid: string,
    eventName: EventName,
    channel: string[] | string,
    viaId: number,
    userId?: string,
    zdTicketId?: string
  ): Promise<GetTicketResponse> {
    const getTicketRes = await this.getTicket(taskSid, zdTicketId);
    const { ticketId, isClosed } = getTicketRes;

    if (!ticketId) {
      const createNewTicket = Boolean(crmIntegration.getIntegrationParameter('invalidTicketNumber.createNewTicket'));
      if (createNewTicket && eventName === 'acceptTask') {
        const tags = channel ? (Array.isArray(channel) ? channel : [channel]) : undefined;
        const newTicket = {
          ticket: {
            requester_id: userId,
            comment: {
              body: crmIntegration.getIntegrationParameter('newTicketDescription') || 'New ticket',
              public: false,
            },
            subject: crmIntegration.getIntegrationParameter('newTicketSubject') || 'New ticket',
            status: 'new',
            via_id: viaId,
            ...(isClosed && zdTicketId && { via_followup_source_id: Number(zdTicketId) }),
            tags,
          },
        };

        const req = await this.client.request({
          url: '/api/v2/tickets.json',
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(newTicket),
        });

        const newTicketId = get(req, 'ticket.id');

        if (newTicketId) {
          this.newTickets[taskSid] = newTicketId;
        }

        return {
          ticketId: newTicketId,
          isClosed: false,
        };
      }
      return {
        ticketId: zdTicketId && isClosed ? zdTicketId : undefined,
        isClosed,
      };
    }

    return {
      ticketId,
      isClosed: false,
    };
  }

  private async getOrCreateUser(channelType: string, eventName: EventName, userPhone?: string) {
    if (channelType === 'web' || channelType === 'custom') {
      // Don't create new users if we don't have a phone number or a verified unique information
      return;
    }

    let userId = await this.findUserByPhone(userPhone);
    const createNewUser = Boolean(crmIntegration.getIntegrationParameter('invalidTicketNumber.createNewUser'));

    if (userPhone && !userId && createNewUser && eventName === 'acceptTask') {
      let newUserResponse;
      try {
        newUserResponse = await this.createNewUser(userPhone);
      } catch (err) {
        newUserResponse = undefined;
      }
      userId = get(newUserResponse, 'user.id');

      if (userId) {
        this.newUsers[userPhone] = userId;
      }
    }

    return userId;
  }

  private createNewUser(phoneNumber: string) {
    const newUser = {
      user: {
        name: phoneNumber,
        phone: phoneNumber,
      },
    };

    return this.client.request({
      url: '/api/v2/users.json',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(newUser),
    });
  }

  private async navigateToUserScreen(userId?: string, isValidTicketId?: boolean) {
    if (!userId) {
      return;
    }

    const navigateToUser = Boolean(
      crmIntegration.getIntegrationParameter(
        isValidTicketId ? 'validTicketNumber.navigateToUser' : 'invalidTicketNumber.navigateToUser'
      )
    );

    if (navigateToUser) {
      await this.client.invoke('routeTo', 'user', userId.toString());
    }
  }

  private async navigateToTicketScreen(ticketId?: string, isValidTicketId?: boolean) {
    if (!ticketId) {
      return;
    }

    const navigateToTicket = Boolean(
      crmIntegration.getIntegrationParameter(
        isValidTicketId ? 'validTicketNumber.navigateToTicket' : 'invalidTicketNumber.navigateToTicket'
      )
    );

    if (navigateToTicket) {
      await this.client.invoke('routeTo', 'ticket', ticketId.toString());

      // Force updating selectedTicket after the navigation only if not set
      if (this.selectedTicket.ticketId) {
        return;
      }

      const getInstances = await this.client.get('instances');
      const clientInstances = getInstances && getInstances.instances;

      for (const clientInstance in clientInstances) {
        const clientTicketId = clientInstances[clientInstance].ticketId;
        if (Number(clientTicketId) === Number(ticketId)) {
          this.selectedTicket = {
            ticketId,
            instanceGuid: clientInstance,
          };
          break;
        }
      }
    }
  }

  private async clickToDial({ phone, type, ticketId, instanceGuid }: any) {
    if (!phone) {
      return;
    }

    const extraTaskAttributes: any = {};

    if (type === 'ticket') {
      extraTaskAttributes['zd_ticket_id'] = ticketId;
      this.selectedTicket = { ticketId, instanceGuid };
    } else if (type === 'user') {
      // If the click to dial is triggered from the user sidebar widget, create a ticket first
      this.selectedTicket = {};
    }

    // Trigger the outbound call in Flex
    await crmIntegration.performOutboundCall(phone, extraTaskAttributes);
  }

  private async findUserByPhone(phoneNumber?: string): Promise<string | undefined> {
    if (!phoneNumber) {
      return;
    }

    if (this.newUsers[phoneNumber]) {
      return this.newUsers[phoneNumber];
    }

    let userData;
    try {
      userData = await this.client.request(`/api/v2/users/search.json?query=phone:"${phoneNumber}"`);
    } catch (err) {
      userData = undefined;
    }
    const users = get(userData, 'users', []);

    if (users.length) {
      return users[0].id;
    }
  }

  /**
   * log
   *
   * Log some useful information to the ticket "Internal notes"
   */
  private async log(task: ITask, logDescription: LogDescription) {
    if (!logDescription) {
      return;
    }

    const { taskChannelUniqueName, sid } = task;
    const { prettifyMessage, message, chatMessages } = logDescription;
    const taskLoggingOnStart = Boolean(crmIntegration.getIntegrationParameter('taskLogging.onStart'));
    const taskLoggingOnCompleted = Boolean(crmIntegration.getIntegrationParameter('taskLogging.onCompleted'));
    const taskLoggingOnSwitch = Boolean(crmIntegration.getIntegrationParameter('taskLogging.onSwitch'));
    const inboundTextRecordings = Boolean(crmIntegration.getIntegrationParameter('textRecordings.inbound'));
    const taskTicketId = get(task, 'attributes.zd_ticket_id');
    const { ticketId } = await this.getTicket(sid, taskTicketId);

    if (!ticketId) {
      return;
    }

    // Log chat to internal note
    if (message.status === MessageStatus.COMPLETED && inboundTextRecordings && taskChannelUniqueName !== 'voice') {
      this.backgroundApp.trigger('ticket.log', {
        ticketId,
        message: `Chat conversation:\n${chatMessages || ''}`,
      });
    }
    if (message.status === MessageStatus.STARTED && !taskLoggingOnStart) {
      return;
    }
    if (message.status === MessageStatus.COMPLETED && !taskLoggingOnCompleted) {
      return;
    }
    if (message.status === MessageStatus.MOVED_TO && !taskLoggingOnSwitch) {
      return;
    }

    if (
      this.selectedTicket.ticketId &&
      Number(this.selectedTicket.ticketId) === Number(ticketId) &&
      message.status === MessageStatus.MOVED_TO
    ) {
      // Don't record the ticket-select interaction when the agent is already on the targeted ticket
      return;
    }

    this.backgroundApp.trigger('ticket.log', {
      ticketId,
      message: prettifyMessage,
      status: message.status,
    });
  }
}