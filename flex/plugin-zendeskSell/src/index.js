import * as FlexPlugin from 'flex-plugin';
import ZendeskSellPlugin from './ZendeskSellPlugin';

FlexPlugin.loadPlugin(ZendeskSellPlugin);
